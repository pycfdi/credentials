# FIEL y CSD
Los certificados FIEL y CSD son archivos entregados por el SAT para realizar disntitas operaciones.
Técnicamente son muy similares (ver [diferencias_fiel_csd.md](diferencias_fiel_csd.md)), in embargo tienen "facultades" muy disntintas en el mundo real

## Firma Electrónica Avanzada (FIEL)
Este certificado es el equivalente a una firma autógrafa por parte del emisor, incluso llegando a tener la válidez com una firma ante notario.
Sus usos son muy variados, pues sirve para autenticar que algo es aceptado por una persona, por ejemplo para firmar un contrato de compra-venta, procedimientos ante el gobierno o instituciones públicas (ej: Tramites ante la SEP), y, uno de los que más nos interesa en este ámbito, la solicitud de descarga másiva de CFDI's hacía el SAT.

Normalmente solo hay uno por persona (Verificar este dato)

**IMPORTANTE** La FIEL es un documento que se debe resguardar con mucho celo, quién lo posea puede hacerse pasar digitalmente por la persona relacionada a la misma.

## Certificado de Sello Digital (CSD)
Este certificado no sirve para identificar a una persona en si mismo, sólo es válido pra realizar ciertas operaciones relacionadas a los CFDI's; por ejemplo:
* Sellar un CFDI
* Realizar una cancelación de CFDI

Normalmente se emiten tantos CSD's como se requieran, por ejemplo: uno por cada sistema que realiza operaciones con CFDI's, o inclusive uno por cada punto de venta o sistema de emisión. La ventaja de hacer esto es que, si uno llega a car en las manos equivocadas, se puede revocar y crear otro.

## Notas generales
Los certificados cuentan con un par de fechjas que indican la ventana de tiempo en la que son válidos, sin embargo, el SAT tiene la facultad de revocarlos en cualquier momento (a petición del responsable), sin embargo, de momento, no hay una forma automática de conocer si el certificado es vigente o no, ni mucho menos conocer la fecha de revocación.
