# Manejo de Archivos
Aquí se explican los distintos archivos que entrega el SAT (tanto [FIEL como CSD](fiel_csd.md))

## Notas previas
### Tipos de "encodificaciones" (DER vs PEM)
Los archivos entregados por el SAT están en una "codificación" llamada `DER`, esta codificación genera un archivo binario, estos archivos **no** son típicamente aceptados por los distintos lenguajes de programación (entre ellos, python); para esto se requiere su equivalente en formato `PEM`, el cual es un archivo te texto plano que puede contener multiples secciones (identificadas con la cabecera `-----BEGIN $TYPE-----`, dónde $TYPE indica el tipo de contenido), el contenido en cada sección es codificado en base64 y con un máximo de 64 carácteres por fila.

### Tipo de archivo para certificados (X509)
El archivo de certificado que entrega el SAT son tipo X509 (codificados en DER), dentro de estos se encuentra la siguiente información:
* Serial Number: Lo entrega en hexadecimal, falta convertirlo a ASCII para que coincida con la información del SAT)
* Issuer: Indica quién emite el certificado, incluyendo: nombre, correo, dómicilio, código postal, ciudad, estado, localidad y RFC.
* Validity:
  * Not Before: Indica desde que fecha comienza a ser válido el certificado
  * Not After:  Indica hasta que fecha es válido el certificado
* Subject: Indica la identidad de quien certifica, incluyendo: Nombre, ciudad, correo, RFC y número de serie
* Llave pública: Llave RSA, utilizada para la verificación de las firmas

[Ejemplo de esta información](certificate_info.txt)

### Tipo de archivo para llaves privadas (PKCS8 vs Traditional)
El archivo `*.key` que entrega el SAT es tipo `PKCS8` (codificado en DER), este contiene la llave privada y **encriptada** con la contraseña de texto plano. Se puede convertir este archivo a formato PEM, sin embargo durante este proceso se pierde la encriptación (el archivo resultante es del tipo `Traditional`), por lo que se recomienda volver a encriptar y formar un nuevo archivo `PKCS8` con una nueva (puede ser la misma) contraseña (aquí puede ser en formato DER o PEM).
Cabe recalcar que la generación de archivos `PKCS8` no es determinista, por lo que los archivos generados son *equivalentes* al original, pero no son idénticos.

## Archivos originales
1. *.cer (Binario, DER, X509)
   1. Archivo certificado
   2. Contiene información adicional sobre el certificado
   3. Contiene la llave publica
2. *.key (Binario, DER, pkcs8, encriptado)
   1. Archivo de llave privada
3. passphrase
   1. Texto plano usado para encriptar la llave privada

## envars
Estas son las variables de entorno que se utilziaron para la generación de los archivos "secundarios" a partir de los originales
```bash
CER_DER=*.cer
CER_PEM=$CER_DER.pem
PUB_PEM=$CER_PEM.pub
PRIV_DER=*.key
TRADITIONAL=$PRIV_DER.traditional
ENCRYPTED_DER=$TRADITIONAL.der
ENCRYPTED_PEM=$TRADITIONAL.pem
ORIGINAL=*.txt
SIGNATURE=$ORIGINAL.signature
```

## Acciones X509
### Obtener info del certificado
> `openssl x509 -in $CER_DER --inform der -noout -text`

### DER ➜ PEM
Los archivos Binarios tipo DER, pueden ser convertidos a DER con el siguiente comando
> `openssl x509 -in $CER_DER -inform DER -out $CER_PEM -outform PEM`

### PEM ➜ DER
Los archivos de texto PEM pueden ser convertidos a DER con el siguiente comando

**Nota:** El archivo genrado es identico al original DER
> `openssl x509 -in $CER_PEM -inform PEM -out $CER_DER -outform DER`

### PEM ➜ PEM(pubkey)
  > `openssl x509 -in $CER_PEM -noout -pubkey > $PUB_PEM`

### DER ➜ PEM(pubkey)
  > `openssl x509 -in $CER_PEM -inform DER -noout -pubkey > $PUB_PEM`

## Acciones pkcs8
### DER ➜ PEM(traditional) (Quitar passprhase)
Los archivos Binarios tipo DER, pueden ser convertidos a DER con el siguiente comando

**Nota:** Esto quita la contraseña en caso de tenerla
> `openssl pkcs8 -in $PRIV_DER -inform DER -out $TRADITIONAL -outform PEM`

### PEM(traditional) ➜ DER(pkcs8) (Encriptar DER)
**Nota:** Esto genera un archivo equivalente al original, pero no son identicos (Cada encriptación genera cadenas distitnas)
> `openssl pkcs8 -in $TRADITIONAL -out $ENCRYPTED_DER -outform DER -topk8`

### PEM(traditional) ➜ PEM(pkcs8) (Encriptar PEM)
**Nota:** Esto genera un archivo de texto con la claveprivada encriptada con una nueva passphrase
> `openssl pkcs8 -in $TRADITIONAL -out $ENCRYPTED_PEM -topk8`


## Acciones dgst
### Firmar
**Nota:** Las firmas siempre son idenicas si la llave privada es la misma (no importa el formato o si están o no encriptadas)
> `openssl dgst -sha256 -sign $ENCRYPTED_PEM -out $SIGNATURE $ORIGINAL`

### Verificar
  > `openssl dgst -sha256 -verify $PUB_PEM -signature $SIGNATURE $ORIGINAL`
