## Diferencias Técnias entre FIEl y CSD

No existe una forma "oficial" de identificar si un archivo `*.cer` es una FIEL o un CSD, sin embargo, se encuentra la siguiente información NO-OFICIAL: <https://solucionfactible.com/sfic/capitulos/timbrado/CSD.jsp#diferenciar?>
En esta se menciona que los CSD contienen un atributo llamado `OU` que indica el nombre "alias" de un CSD, en cambio las FIEl no contienen dicho nodo. Esta es la información que utiliza este proyecto para determinar el tipo de certificado.

Cabe mencionar que no son las únicas diferencias, pareciera que los archivos tipo FIEL además tienen dentro de su nodo `X509v3 Key Usage` las siguientes opciones:
* `Data Encipherment`
* `Key Agreement`
Y dentro de `X509v3 Extended Key Usage` contiene:
* `E-mail Protection`
* `TLS Web Client Authentication`
Sin embargo, se optó por el uso del campo `OU` puesto es más simple de obtener.
