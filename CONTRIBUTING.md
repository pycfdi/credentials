# Contribuciones

Este proyecto sigue las recomendaciones que la organización padre [pyCFDI](https://gitlab.com/pycfdi).
Favor de referirse al documento [CONTRIBUTING.md](https://gitlab.com/pycfdi/doc/-/blob/main/CONTRIBUTING.md)
