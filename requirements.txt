cffi==1.15.0 ; python_version >= "3.7" and python_version < "4.0"
cryptography==39.0.2 ; python_version >= "3.7" and python_version < "4.0"
pycparser==2.21 ; python_version >= "3.7" and python_version < "4.0"
pyopenssl==23.0.0 ; python_version >= "3.7" and python_version < "4.0"
