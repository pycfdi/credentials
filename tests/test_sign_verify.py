import pytest
from hypothesis import assume, given
from hypothesis import strategies as st

from pycfdi_credentials import Certificate, PrivateKey
from pycfdi_credentials.utils import read_file

ALGORITHMS = ["sha256", "sha1", "sha512"]

CERTIFICATES = (
    "FIEL_AAA010101AAA",
    "CSD01_AAA010101AAA",
)


@given(content=st.text())
@pytest.mark.parametrize("cert_name", CERTIFICATES)
@pytest.mark.parametrize("algorithm", ALGORITHMS)
def test_sign_verify(cert_name: str, algorithm: str, content: str):
    """
    Test sign method can sign and verify the signature
    """
    private_key = PrivateKey(
        read_file(f"tests/assets/{cert_name}/private_key.key"), passphrase=b"12345678a"
    )
    content_bytes = content.encode("utf-8")
    signature = private_key.sign(content_bytes, algorithm)
    certificate = Certificate(read_file(f"tests/assets/{cert_name}/certificate.cer"))
    is_valid = certificate.verify(signature, content_bytes, algorithm)
    assert is_valid is True


@given(content=st.text(), fake_content=st.text())
@pytest.mark.parametrize("cert_name", CERTIFICATES)
@pytest.mark.parametrize("algorithm", ALGORITHMS)
def test_sign_verify_wrong(cert_name: str, algorithm: str, content: str, fake_content: str):
    """
    Test sign method can sign and verify the signature
    """
    assume(content != fake_content)
    private_key = PrivateKey(
        read_file(f"tests/assets/{cert_name}/private_key.key"), passphrase=b"12345678a"
    )
    content_bytes = content.encode("utf-8")
    signature = private_key.sign(content_bytes, algorithm)
    certificate = Certificate(read_file(f"tests/assets/{cert_name}/certificate.cer"))
    fake_content_bytes = fake_content.encode("utf-8")
    is_valid = certificate.verify(signature, fake_content_bytes, algorithm)
    assert is_valid is False


@given(content=st.text())
@pytest.mark.parametrize("cert_name", CERTIFICATES)
@pytest.mark.parametrize("algorithm", ALGORITHMS)
def test_sign_consistency(cert_name: str, algorithm: str, content: str):
    private_key = PrivateKey(
        read_file(f"tests/assets/{cert_name}/private_key.key"), passphrase=b"12345678a"
    )
    content_bytes = content.encode("utf-8")
    signature = private_key.sign(content_bytes, algorithm)
    signature_2 = private_key.sign(content_bytes, algorithm)
    assert signature == signature_2
